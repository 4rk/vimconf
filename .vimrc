" CONFIG

set nocompatible              " Cancel Vi compatibility
filetype off                  " required


set splitbelow
set splitright


" DISPLAY

set tabstop=4
set expandtab
set shiftwidth=4
set autoindent
set smartindent

set title

set number
set ruler
set cursorline

set scrolloff=4


" BEEP

set visualbell
set noerrorbells




" PLUGINS

set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

Plugin 'VundleVim/Vundle.vim'

" Plugin 'severin-lemaignan/vim-minimap'

" Plugin 'nathanaelkane/vim-indent-guides'

Plugin 'Valloric/YouCompleteMe'

" Plugin 'ajh17/VimCompletesMe'



Plugin 'vim-airline/vim-airline'
" Plugin 'itchyny/lightline.vim'

Plugin 'gcavallanti/vim-noscrollbar'

" Plugin 'bassnode/vim-google-play'


call vundle#end()            " required
filetype plugin indent on    " required




" autocmd VimEnter * Minimap 

" let g:minimap_highlight='Include'


" let g:indent_guides_enable_on_vim_startup = 1
" let g:indent_guides_guide_size = 1
" let g:indent_guides_color_change_percent = 3




" FINAL PARAMS

syntax enable
set antialias
set background=dark
colorscheme material-theme


" clear cursorline and re-put

hi clear CursorLine
augroup CLClear
    autocmd! ColorScheme * hi clear CursorLine
augroup END
hi CursorLineNR cterm=bold
augroup CLNRSet
    autocmd! ColorScheme * hi CursorLineNR cterm=bold
augroup END
highlight LineNr ctermfg=darkblue

" text progress

function! Noscrollbar(...)
    let w:airline_section_z = '%{noscrollbar#statusline(10,"▒","░")}'
endfunction
call airline#add_statusline_func('Noscrollbar')


" Desactiver les touches directionnelles
map <up> <nop>
map <down> <nop>
map <left> <nop>
map <right> <nop>
imap <up> <nop>
imap <down> <nop>
imap <left> <nop>
imap <right> <nop>



